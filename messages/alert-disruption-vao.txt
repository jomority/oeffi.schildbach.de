title:	Disruption notice
body:	The network "Verkehrsauskunft Österreich" was removed from Öffi at the request of VAO GmbH.

To work around the problem, you can switch Öffi to another network covering your area.
button-neutral:		dismiss
